angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$localStorage,$rootScope,$ionicSideMenuDelegate,$ionicLoading,$http,$ionicPlatform) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
// $scope.$on('$ionicView.enter', function(e) {
  
/*
  $scope.$watch(function () 
  {
    return $ionicSideMenuDelegate.getOpenRatio();
  },
	function (ratio) {
	  if (ratio == 1){
		//alert("hey");
	  }
	  else
	  {
		  //alert ("close");
	  }
	});	
*/

	$scope.$on('$ionicView.enter', function(e) {
		$scope.currentController = $rootScope.State;
	});	
	
	
	$scope.checkGender = function()
	{
		//if ($localStorage.sex == 2)
		//	$scope.showPaymentOptions = false;
		//else
			$scope.showPaymentOptions = true;
		
	}
	
	
	$scope.checkGender();

	
	$scope.getBadgeCount = function()
	{
		$scope.FollowersCount = $rootScope.FollowersCount;
		$scope.FriendsCount = $rootScope.FriendsCount;
		$scope.BlockedCount = $rootScope.BlockedCount;	
	}
	
	$scope.increaseFollowers = function()
	{
		 $timeout(function() {
			$scope.FollowersCount++;
		}, 300);		
		
	}

	//id,type,returned
	$scope.updateCount = function()
	{
		if ($localStorage.userid)
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
			send_params = 
			{
				"user" : $localStorage.userid
			}			
			$http.post($rootScope.Host+'/get_badge_count.php',send_params)
			.success(function(data, status, headers, config)
			{
				$scope.FollowersCount = data[0].followers;
				$scope.FriendsCount = data[0].friends;
				$scope.BlockedCount = data[0].blocked;
				
				//alert ($scope.FollowersCount)
				//alert ($scope.FriendsCount)
				//alert ($scope.BlockedCount)
				
				$rootScope.FollowersCount = $scope.FollowersCount;
				$rootScope.FriendsCount = $scope.FriendsCount;
				$rootScope.BlockedCount =  $scope.BlockedCount;

				$scope.getBadgeCount();		
		
			})
			.error(function(data, status, headers, config)
			{

			});		

			
		}
		
		//$scope.updateCount();


		
		
		/*
		if (id == 3)
		{
			if (type == "remove")
			{
				if ($rootScope.BlockedCount > 0)
				$rootScope.BlockedCount--;
			
				if (returned == 0)
				$rootScope.FriendsCount++;
			
				if (returned == 1)
				$rootScope.FollowersCount++;			
			}
			else
			{
				
			}
		}
		*/
		
		
	}
	
	$ionicPlatform.on("resume", function(event) {
		$scope.updateCount();
	});
			
	$scope.getUserSex = function(gender)
	{
		var usergender = "";
		
		if (gender == 1)
		{
			usergender = "זכר";
		}
		else
		{
			usergender = "נקבה";
		}
		
		return usergender;
		
	}
	
	$scope.updateUserSettings = function()
	{
		$scope.username = $localStorage.name;
		$scope.userid = $localStorage.userid;
		
    	if (!$localStorage.image)
			$scope.profilepicture = "img/avatar.png";
		else
			$scope.profilepicture = $rootScope.Host+$localStorage.image;

		$scope.getBadgeCount();
	
	}
	
	$scope.updateUserSettings();
	
	
	
	//alert ($scope.username)
	
	/*
	$rootScope.$watch('profilepicture', function() 
	{
    	if (!$localStorage.image)
		$scope.profilepicture = "img/avatar.png";
		else
		{
			$scope.profilepicture = $rootScope.Host+$localStorage.image;
		}
	});
	*/
	
	
	
	$scope.getUserMoney = function(money)
	{
		var usermoney = "";
		
		if (money == 1)
		{
			usermoney = "חלש";
		}
		else if (money == 2)
		{
			usermoney = "בינוני";
		}
		else
		{
			usermoney = "חזק";
		}
		
		return usermoney;
		
	}	  
	
	/*
	$rootScope.$watch('userid', function() 
	{
    	 $scope.userid = $localStorage.userid;
	});
	*/
	

  $scope.logOut = function()
  {

		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
		send_params = 
		{
			"user" : $localStorage.userid
		}			
		$http.post($rootScope.Host+'/logout.php',send_params)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();
			$rootScope.MatcheCounter = 0;
			$localStorage.userid = '';
			$localStorage.name = '';
			$localStorage.phone = '';
			$localStorage.sex = '';
			$localStorage.favorite = '';
			$localStorage.money = '';
			$localStorage.calls = '';
			$localStorage.confirmed = '';
			$localStorage.notifications = '';
			$localStorage.image = '';
			$rootScope.userid = "";
			$rootScope.profilepicture = "";		

			$rootScope.FollowersCount  = 0;
			$rootScope.FriendsCount = 0;
			$rootScope.BlockedCount = 0;

			$scope.FollowersCount  = 0;
			$scope.FriendsCount = 0;
			$scope.BlockedCount = 0;
			
			window.location ="#/app/login";
		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();

		});		  

  }

  
 // });
})


.controller('LoginRegisterCtrl', function($scope, $stateParams,$localStorage,$rootScope,$http,$ionicPopup,$ionicLoading,$ionicHistory,$ionicSideMenuDelegate,$ionicModal) {



	


	$ionicSideMenuDelegate.canDragContent(false);
	console.log("LogIn")
	$scope.GlobalCounter = 1257;
	$scope.loginStatus = "";
	if ($localStorage.userid && $localStorage.confirmed == 1 )
	{
		 $ionicHistory.nextViewOptions({
			disableBack: false
		  });
	  
		window.location ="#/app/matches/0";
	}


	
	//$scope.navTitle='<p class="title-image">'+$localStorage.name+ ' ' +$localStorage.userid+   '</p>';
	$scope.navTitle='<p class="title-image">דיסקרטי</p>';

	$scope.fields = 
	{
		"phone" : "",
		"smscode" : ""
	}
	
	
	$scope.openSmsModal = function()
	{
	  $ionicModal.fromTemplateUrl('templates/sms_modal.html', {
		scope: $scope
	  }).then(function(smsModal) {
		$scope.smsModal = smsModal;
		$scope.smsModal.show();
	  });			
	}
	
	//$scope.openSmsModal();
	
	
	$scope.closeSmsModal = function()
	{
		$scope.smsModal.hide();
	}
	
	$scope.resendCode = function()
	{
		if ($scope.fields.phone)
		{
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
			send_params = 
			{
				"phone" : $scope.fields.phone,
			}			
			$http.post($rootScope.Host+'/resend_code.php',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();

				$ionicPopup.alert({
				title: 'קוד נשלח בהצלחה',
				template: ''
				});	

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();

			});				
		}
	}
	
	

	$scope.confirmCode = function()
	{
		if ($scope.fields.smscode =="")
		{
			$ionicPopup.alert({
			title: 'יש להזין קוד אימות',
			template: ''
			});				
		}
		else
		{
			
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
			send_params = 
			{
				"phone" : $scope.fields.phone,
				"code" : $scope.fields.smscode
			}			
			$http.post($rootScope.Host+'/verify_code.php',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				
			    if (data[0].verified == 0)
				{
					$ionicPopup.alert({
					title: 'קוד אימות לא תקין יש לנסות שוב',
					template: ''
					});		
					
					$scope.fields.smscode = '';
			
				}
				else
				{
					$scope.smsModal.hide();
					$localStorage.confirmed = 1;
					window.location ="#/app/matches/0";			
				}

				

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();

			});	
		}	
	}
	
	$scope.loginBtn = function()
	{
		
		console.log("loginBtn")
		if ($scope.fields.phone =="")
		{
			$ionicPopup.alert({
			title: 'יש להזין מספר טלפון',
			template: ''
			});				
		}
		else
		{
			
			
			$rootScope.userid = "";
			$rootScope.profilepicture = "";
			
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
			send_params = 
			{
				"phone" : $scope.fields.phone,
				"pushid" : $rootScope.pushId
			}			
			$http.post($rootScope.Host+'/login.php',send_params)
			.success(function(data, status, headers, config)
			{
				//alert ($localStorage.image)
				console.log("login " , data)
				$ionicLoading.hide();
				
				
				$scope.loginStatus = data[0].status;
				//alert ($scope.loginStatus)
				
				$rootScope.userid = data[0].userid;
				$localStorage.userid = data[0].userid;
				$localStorage.name = parseInt(data[0].userid) + $scope.GlobalCounter;
				$localStorage.phone = data[0].phone;
				$localStorage.sex = data[0].sex;
				$localStorage.confirmed = data[0].confirmed;
				
				//$scope.checkGender();
				
				//alert (data[0].sex);
				//alert ($localStorage.sex)
				$localStorage.favorite = data[0].favorite;
				$localStorage.money = data[0].money;
				$localStorage.calls = data[0].calls;
				$localStorage.notifications = data[0].notifications;
				$localStorage.image = data[0].image;
				$rootScope.profilepicture = data[0].image;
				
				
				$rootScope.FollowersCount = data[0].followers;
				$rootScope.FriendsCount = data[0].friends;
				$rootScope.BlockedCount = data[0].blocked;
				
				
				//alert ($localStorage.sex)
				if ($localStorage.sex =="0" && $scope.loginStatus == 0)
				{
					console.log("login1" , data)
					send_params = 
					{
						"user" : $localStorage.userid,
						"sex" :1,
						"favorite" :2,
						"money" : 2,
						"calls" : 1,
						"notifications" :1,
						"image" :""
					}			
					console.log("send_params" , send_params)
					$http.post($rootScope.Host+'/update_personal.php',send_params)
					.success(function(data, status, headers, config)
					{
						console.log("login2" , data)
						$ionicLoading.hide();
						
						//alert ($scope.fields.calls);
						//alert ($scope.fields.notifications)
		
							$localStorage.sex = 1;
					
							$localStorage.favorite = 2;
					
							$localStorage.money = 2;
						
							$localStorage.calls = 1;
					
							$localStorage.notifications = 1;
					
							$localStorage.image = "";
						
						//$scope.getMatches();
						//window.location ="#/app/matches/0";
				
					})
					.error(function(data, status, headers, config)
					{
						$ionicLoading.hide();
		
					});	
				}
				$scope.checkGender();
				$scope.updateUserSettings();
				$scope.updateCount();

				
				if ($localStorage.confirmed == 0)
				{
					$scope.openSmsModal();
				}
				else
				{
					$localStorage.confirmed = 1;
					window.location ="#/app/matches/0";
				}
			
				
			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();

			});				
		}
	}
	
})

.controller('SettingsCtrl', function($scope, $stateParams,$localStorage,$rootScope,$http,$ionicPopup,$ionicLoading,$cordovaCamera,$timeout,$ionicSideMenuDelegate) {



	
	//$scope.$on('$ionicView.enter', function(e) {
	console.log("Settings")
	//$scope.navTitle='<p class="title-image">'+$localStorage.name+ ' ' +$localStorage.userid+   '</p>';
	$scope.navTitle='<p class="title-image">דיסקרטי</p>';
	$scope.updateUserSettings();
	
	
	//alert ($rootScope.State);
	
	$scope.getUserProfile = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
		send_params = 
		{
			"user" : $localStorage.userid
		}			
		$http.post($rootScope.Host+'/get_profile.php',send_params)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();
			//alert (data[0].image);
			
			if (data[0].image)
			{
				$localStorage.image = data[0].image;
				$scope.profilepicture = $rootScope.Host+data[0].image;
				$rootScope.profilepicture = $scope.profilepicture;
			}
			else
			{
				$localStorage.image = "";
				$scope.profilepicture = "";
				$rootScope.profilepicture = "img/avatar.png";
			}

		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();

		});			
	}
	
	
	//$scope.getUserProfile();
	
	
	
	
	 //$timeout(function() {
		//$ionicSideMenuDelegate.$getByHandle('testing').toggleRight();
	//}, 3000);

			
	$scope.genderSettings = function()
	{
		//$timeout(function() 
		//{
			if ($localStorage.sex == 1)
			{
				$scope.selectedMale = "img/settings/man2.png";
				$scope.selectedFemale = "img/settings/woman1.png";
				
			}
			else
			{
				$scope.selectedMale = "img/settings/man1.png";
				$scope.selectedFemale = "img/settings/woman2.png";
				
			}	
		//}, 300);		
	}
	
	$scope.intersted = function()
	{
		//$timeout(function() 
		//{
			if ($localStorage.favorite == 1)
			{
				$scope.favoriteMale = "img/settings/man2.png";
				$scope.favoriteFemale = "img/settings/woman1.png";
				
			}
			else
			{
				$scope.favoriteMale = "img/settings/man1.png";
				$scope.favoriteFemale = "img/settings/woman2.png";
				
			}			
		//alert ($localStorage.favorite);
		//alert ($scope.favoriteMale);
		//}, 300);
	}
	
	

		$scope.genderSettings();
		$scope.intersted();

			

	
	
	$scope.changeGender = function(type)
	{
		$scope.fields.sex = type;
		$localStorage.sex = type;
		$scope.genderSettings();
		$scope.saveBtn();
		//$rootScope.$broadcast("refresh");
		//$scope.getMatches();
	}

	$scope.changeFavorite = function(type)
	{
		console.log("c1")
		$scope.fields.favorite = type;
		$localStorage.favorite = type;
		$scope.intersted();
		$scope.saveBtn();
		$rootScope.MatcheCounter = 0;
		$rootScope.$broadcast("refresh");
		//$scope.getMatches();
	}		
		
		
		
	$scope.settings = function()
	{
		$timeout(function() 
		{
			if ($localStorage.image)
				$scope.profilepicture = $rootScope.Host+$localStorage.image;
			else
				$scope.profilepicture = "";
		
			$scope.calls = ($localStorage.calls == 0 ? true : false);
			$scope.notifications = ($localStorage.notifications == 0 ? true : false);
			
			//alert ($localStorage.calls);
			console.log("c2")
			$scope.fields = 
			{
				"sex" : $localStorage.sex,
				"favorite" : $localStorage.favorite,
				"money" : $localStorage.money,
				"calls" : $scope.calls,
				"notifications" : $scope.notifications
			}		
		
		}, 300);

			
			
			
	}
	
	$scope.settings();
	
	
	$rootScope.$watch('profilepicture', function() 
	{
    	if (!$localStorage.image)
		$scope.profilepicture = "img/avatar.png";
		else
		{
			$scope.settings();
			$scope.genderSettings();
			$scope.intersted();

		
		}
	});
	
	
	
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 500,
				targetHeight: 500,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 500,
				targetHeight: 500,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.Host+'/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    		/*
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	
			*/
			
		});
		
		$scope.onUploadSuccess = function(data)
		{
			//$ionicLoading.hide();
			 $timeout(function() {
				if (data.response) {
				
					$scope.newimage = data.response;
					$scope.profilepicture = $rootScope.Host+data.response;	

					$scope.saveBtn();					
					//$scope.showimage = true;
				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			$ionicLoading.hide();
			alert("onUploadFail : " + data);
		}
    }	
	
	$scope.saveBtn = function()
	{
		if ($scope.newimage)
			$scope.uploadedimage = $scope.newimage;
			else
			$scope.uploadedimage = $localStorage.image;
		
		if ($scope.fields.sex =="0")
		{
			$ionicPopup.alert({
			title: 'יש לבחור מין',
			template: ''
			});	
		}
		else if ($scope.fields.favorite =="0")
		{
			$ionicPopup.alert({
			title: 'יש לבחור אשמח להכיר',
			template: ''
			});	
		}
		/*
		else if ($scope.fields.money =="0")
		{
			$ionicPopup.alert({
			title: 'יש לבחור מעמד כלכלי',
			template: ''
			});	
		}		
		*/
		else if ($scope.uploadedimage =="")
		{
			$ionicPopup.alert({
			title: 'יש לבחור תמונה',
			template: ''
			});	
		}
		else
		{
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';


			$scope.savecalls = ($scope.fields.calls == true ? 0 : 1);
			$scope.savenotification = ($scope.fields.notifications == true ? 0 : 1);
	
			//alert ($scope.savecalls)
			//alert ($scope.savenotification)
	
			send_params = 
			{
				"user" : $localStorage.userid,
				"sex" : $scope.fields.sex,
				"favorite" : $scope.fields.favorite,
				"money" : $scope.fields.money,
				"calls" : $scope.savecalls,
				"notifications" : $scope.savenotification,
				"image" : $scope.uploadedimage
			}			
			


		
			$http.post($rootScope.Host+'/update_personal.php',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				
				//alert ($scope.fields.calls);
				//alert ($scope.fields.notifications)
				console.log("c3")
				$localStorage.sex = $scope.fields.sex;
				$localStorage.favorite = $scope.fields.favorite;
				$localStorage.money = $scope.fields.money;
				$localStorage.calls = $scope.savecalls;
				$localStorage.notifications = $scope.savenotification;
				$localStorage.image = $scope.uploadedimage;
				
				//$scope.getMatches();
				window.location ="#/app/matches/0";
				

				
			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();

			});	
		}
		
	}
	

// });
})

.controller('MatchesCtrl', function($scope, $stateParams,$localStorage,$rootScope,$http,$ionicPopup,$ionicLoading,$ionicSideMenuDelegate,$ionicModal,$ImageCacheFactory,$cordovaGeolocation,$ionicPlatform,$timeout) {
	
	$scope.host = $rootScope.Host;
	$scope.followers = 0;
	$scope.friends = 0;
	$scope.MatchesId = parseInt($stateParams.itemId);
	$scope.MatchesLenth = 0;
	//$scope.navTitle='<p class="title-image">'+$localStorage.name+ ' ' +$localStorage.userid+   '</p>';
	$scope.navTitle='<p class="title-image">דיסקרטי</p>';
	$scope.showNotification = false;
	$scope.GlobalCounter = 1257;

	if (!$localStorage.userid)
		window.location ="#/app/login";


		

	$rootScope.$on("refresh", function (event, args) {
		$scope.getUserLocation();
	});
	
	if ($scope.MatchesId == -1)
	{
		$scope.MatchesId = 0;
		$rootScope.MatcheCounter = 0;
	}
	
	$scope.fields = 
	{
		"location_long" : "",
		"location_lat" : ""
	}
	
	$rootScope.$watch('pushMessage', function() 
	{		  
		  //$scope.showNotification = false;
	});
	
	$rootScope.$on('newpushmsg', function(event, args) {
		

		

		
		$scope.pushText = args.additionalData.text;
		$scope.pushImage = $rootScope.Host+args.additionalData.userimage; //$rootScope.Host+"uploads/083116-110859.jpeg"; 
		$scope.pushUserId = 171; //args.additionalData.userid+$scope.GlobalCounter;
		
		//alert ($scope.pushImage)
		
		  $timeout(function() 
		  {
			$scope.showNotification = true;
			$scope.updateCount();
			//$rootScope.pushContent = args;
			//$scope.pushContent = '<div>התקבלה הודעה חדשה: <img src="'+$rootScope.Host+scope.pushImage+'"></div>';
			 
		  }, 300);	
		  
		  
		  $timeout(function() 
		  {
			$scope.showNotification = false;
			$scope.pushContent = '';
			 
		  }, 5000);			 
	
	});

	


/*
	$ionicLoading.show({
      template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
    });
	

  $scope.$on('imageloaded', function(events, args){
	$ionicLoading.hide();
  })
*/
	
	$scope.reportFields = 
	{
		"choice" : "",
		"text" : ""
	}
	
	//alert ($scope.MatchesId)





  
	$scope.getMatches = function(lat,lng)
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
		if($localStorage.favorite == 0)
		$localStorage.favorite = 2;
		
		send_params = 
		{
			"user" : $localStorage.userid,
			"favorite" : $localStorage.favorite,
			"lat" : lat,
			"lng" : lng
		}
		
		console.log("send_params : " , send_params)
			
		$http.post($rootScope.Host+'/get_matches.php',send_params)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();
			//alert (data.length);
			if (data.length == 0)
			{
				$scope.clearMatches();
			}
			console.log("matches : " , data)
			$rootScope.matches = data;
			$scope.MatchesLenth = data.length;
			$scope.showMatches = data[$scope.MatchesId];
		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();

		});			
	}

	$scope.updateUserLocation = function(lat,lng)
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		send_params = 
		{
			"user" : $localStorage.userid,
			"lat" : lat,
			"lng" : lng
		}
		
		console.log("send_params : " , send_params)
			
		$http.post($rootScope.Host+'/update_location.php',send_params)
		.success(function(data, status, headers, config)
		{
			 $scope.getMatches(lat,lng);
		})
		.error(function(data, status, headers, config)
		{
			 $scope.getMatches(lat,lng);
		});			
	}

	


			
	$scope.getUserLocation = function()
	{
		
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	
				
	  /*
		var options = {
		  enableHighAccuracy: false,
		  timeout: 10000,
		  //maximumAge: 0
		};

		function success(pos) {
		  var crd = pos.coords;
		  alert (crd.latitude);
		   alert (crd.longitude);

		  console.log('Your current position is:');
		  console.log('Latitude : ' + crd.latitude);
		  console.log('Longitude: ' + crd.longitude);
		  console.log('More or less ' + crd.accuracy + ' meters.');
		};

		function error(err) {
		  console.warn('ERROR(' + err.code + '): ' + err.message);
		};

		navigator.geolocation.getCurrentPosition(success, error, options);
	  */

		
		
		
		if (window.cordova) 
		{
			
			

	  
			cordova.plugins.diagnostic.isLocationEnabled(function(available){
			if (available)
			{
				//alert ("good");
				var posOptions = {timeout: 10000, enableHighAccuracy: false};
				$cordovaGeolocation
				.getCurrentPosition(posOptions)
				.then(function (position) {
				  $ionicLoading.hide();
				  $scope.latparm  = position.coords.latitude //here you get latitude
				  $scope.longparm = position.coords.longitude //here you get the longitude

				  $scope.fields.location_long = $scope.longparm;
				  $scope.fields.location_lat = $scope.latparm;
				  
				  
				  $scope.updateUserLocation($scope.fields.location_lat,$scope.fields.location_long);			  
				  //alert ($scope.fields.location_long);
				  //alert ($scope.fields.location_lat);
		  
				 //alert ($scope.latparm);
				 //alert ($scope.longparm);
				 
				 
				  

				}, function(err) {
					$ionicLoading.hide();
					//alert (err)
					$scope.getMatches();
				});
			}
			else
			{
					$ionicLoading.hide();
				
				   var confirmPopup = $ionicPopup.confirm({
					 title: 'שירות מיקום מכובה האם ברצונך לפתוח הגדרות המכשיר?',
					 template: ''
				   });				
				   confirmPopup.then(function(res) {
					if(res) 
					{
						cordova.plugins.diagnostic.switchToLocationSettings();
							
					} 
					 else 
					 {
						 $scope.getMatches();
					 }
				   });	
				//cordova.plugins.diagnostic.switchToLocationSettings();
			}
			}, function(error){
				$ionicLoading.hide();
				$scope.getMatches();
			});

			

		
		}
		else
		{
			$ionicLoading.hide();
			$scope.getMatches();
		}
		
			
	}

	//$scope.getUserLocation();

	

	$ionicPlatform.on("resume", function(event) {
		
		if ($rootScope.State =="app.matches")
		$scope.getUserLocation();
	
	});	

	
	$ionicPlatform.ready(function() {
		
		if($rootScope.MatcheCounter == 0)
		$scope.getUserLocation();
	});

	
	
	//if($rootScope.MatcheCounter == 0)
	//$scope.getUserLocation();



	if ($scope.MatchesId == -1)
	{
		//$scope.MatchesId = 0;
		$scope.getUserLocation();
	}
	
	
	
	else
	{
		$scope.MatchesLenth = $rootScope.matches.length;
		$scope.showMatches = $rootScope.matches[$scope.MatchesId];
	}
	
	
	$scope.goSettings = function()
	{
		$ionicSideMenuDelegate.toggleRight();
	}
	
	$scope.showInfo = function(itemIndex)
	{
		
	   for(var i = 0 ;i <$rootScope.matches.length ;i++)
		{
			if ($rootScope.matches[i].index == itemIndex)
			{
				$scope.profile = $rootScope.matches[i];
			}
		}	
		
		$ionicModal.fromTemplateUrl('templates/info-modal.html', {
			scope: $scope
		  }).then(function(infoModal) {
			$scope.infoModal = infoModal;
			$scope.infoModal.show();
		  }); 
	}
	
	
	$scope.closeModal = function()
	{
		$scope.infoModal.hide();
	}
	
	$scope.reportUser = function()
	{
		 // $scope.rate = {};

		  // An elaborate, custom popup
		   $ionicPopup.show({
			//template: '<input type="text" ng-model="rate.points" >',
			  templateUrl: 'templates/report.html',
			title: 'דיווח על משתמש',
			//subTitle: 'Please use normal things',
			scope: $scope,
			buttons: [
			  { text: 'ביטול' },
			  {
				text: '<b>שליחה</b>',
				type: 'button-positive',
				onTap: function(e) {
					if ($scope.reportFields.choice || $scope.reportFields.text)
					{
						$ionicLoading.show({
						  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
						});	
						
						$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
					
						send_params = 
						{
							"user" : $localStorage.userid,
							"recipent" : $scope.profile.index,
							"choice" : $scope.reportFields.choice,
							"text" : $scope.reportFields.text,
							"host" : $rootScope.Host,
							"user_image" : $localStorage.image,
							"recipent_image" : $scope.profile.image,
							"send" : 1
						}			
						$http.post($rootScope.Host+'/report_user.php',send_params)
						.success(function(data, status, headers, config)
						{
							$ionicLoading.hide();

							
							$ionicPopup.alert({
							title: 'תודה משתמש דווח בהצלחה',
							template: ''
							});								

							$scope.reportFields.choice = '';
							$scope.reportFields.text = '';
							
						})
						.error(function(data, status, headers, config)
						{
							$ionicLoading.hide();

						});	
					}
				}
			  }
			]
		  });
			  
	}
	

		
	
	$scope.heartBtn = function(type,recipent)
	{
		
		//alert (recipent);
		//return;
		
		//if (type == 1)
		$scope.LikeBtn(recipent,type);

		
		if ($rootScope.MatcheCounter < $scope.MatchesLenth-1)
		{
			

			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

	
			//$scope.followers++;
			$rootScope.MatcheCounter++;
			$scope.nextImage =  $rootScope.matches[$rootScope.MatcheCounter].image;
			

				$ImageCacheFactory.Cache([
					$rootScope.Host+$scope.nextImage
				]).then(function(){
					$ionicLoading.hide();
					window.location ="#/app/matches/"+$rootScope.MatcheCounter;	
				},function(failed){
					$ionicLoading.hide();
				});		

			
		}
		else
		{
			
			$scope.clearMatches();

		}

	}
	
	$scope.clearMatches = function()
	{
		   var confirmPopup = $ionicPopup.confirm({
			 title: 'הגעת לסוף ההתאמות , האם ברצונך לחזור להתחלה?',
			 template: ''
		   });
	
		   confirmPopup.then(function(res) {
			if(res) 
			{

			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

				send_params = 
				{
					"user" : $localStorage.userid,
				}
				//console.log(login_params)
				$http.post($rootScope.Host+'/clear_matches.php',send_params)
				.success(function(data, status, headers, config)
				{
					$rootScope.MatcheCounter = 0;
					$scope.getUserLocation();
					window.location ="#/app/matches/0";

				})
				.error(function(data, status, headers, config)
				{

				});								
			} 
			 else 
			 {
			 }
		   });		
	}
	
	
	$scope.LikeBtn = function(recipent,type)
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
		send_params = 
		{
			"user" : $localStorage.userid,
			"recipent" : recipent,
			"type" : type,
			"serverhost" : $rootScope.Host
		}			
		$http.post($rootScope.Host+'/like.php',send_params)
		.success(function(data, status, headers, config)
		{
			if (type == 1)
			{
				//$scope.increaseFollowers();
				$scope.updateCount();
			}
		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});			
	}
	
	$scope.likeUser = function(recipent)
	{                                  
		$scope.LikeBtn(recipent);
		$scope.closeModal();
		
		$ionicPopup.alert({
		title: 'עקבת אחרי המשתמש בהצלחה',
		template: ''
		});			
	}
	
	
	
	
})


.controller('FollowersCtrl', function($scope, $stateParams,$localStorage,$rootScope,$http,$ionicPopup,$ionicLoading,$ionicModal) {

	//$scope.navTitle='<p class="title-image">'+$localStorage.name+ ' ' +$localStorage.userid+   '</p>';
	$scope.navTitle='<p class="title-image">דיסקרטי</p>';
	$scope.FollowersImage = "";

	$scope.host = $rootScope.Host;
	
	$scope.CurrentUserId = $localStorage.userid;

	$scope.getFollowers = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
		send_params = 
		{
			"user" : $localStorage.userid
		}			
		$http.post($rootScope.Host+'/get_followers.php',send_params)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();

			console.log("followers : " , data)
			$scope.FollowersArray = data;
			

			
		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();

		});			
	}
	
	$scope.getFollowers();
	
	$scope.updateAction = function(index,item,id,type)
	{
		//alert (type);
		//return;
		
		//type 0 = friends , 1 = block
		//alert (type);
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
		send_params = 
		{
			"user" : $localStorage.userid,
			"recipent" : item.followers[0].index,
			"type" : type,
			"id" : id,
			"serverhost" : $rootScope.Host
		}			
		$http.post($rootScope.Host+'/update_action.php',send_params)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();
			$scope.FollowersArray.splice(index, 1);

			if (type == 0)
			{
				$scope.alertMsg = "משתמש נוסף לחברים";
				
				$rootScope.$broadcast('newfriend','');
			}
			else
			{
				$scope.alertMsg = "משתמש נחסם בהצלחה";
			}
			
			
			$ionicPopup.alert({
			title: $scope.alertMsg,
			template: ''
			});	
			
			$scope.updateCount();

			
		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();

		});	
	}
	
	$scope.showFollowersModal = function(image)
	{
		$scope.FollowersImage = $rootScope.Host+image;
		
		$ionicModal.fromTemplateUrl('followers-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(followersModal) {
		  $scope.followersModal = followersModal;
		  $scope.followersModal.show();
		 });
	}
	
	$scope.closeFollowersModal = function()
	{
		$scope.followersModal.hide();
	}
	
	
	


	$rootScope.$on('newfollower', function(event, args) {
		
		$scope.getFollowers();
	
	});	
	

})


.controller('FriendsCtrl', function($scope, $stateParams,$localStorage,$rootScope,$http,$ionicPopup,$ionicLoading,$ionicModal,$ionicPlatform,$timeout) {

	//$scope.navTitle='<p class="title-image">'+$localStorage.name+ ' ' +$localStorage.userid+   '</p>';
	$scope.navTitle='<p class="title-image">דיסקרטי</p>';
	$scope.FriendsImage = "";

	$scope.host = $rootScope.Host;
	//alert ($localStorage.userid);



	
	$scope.getFriends = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
		send_params = 
		{
			"user" : $localStorage.userid
		}			
		$http.post($rootScope.Host+'/get_friends.php',send_params)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();

			console.log("friends : " , data)
			$scope.FriendsArray = data;
			

			
		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();

		});			
	}
	
	$scope.getFriends();
	
	$scope.blockUser = function(index,id)
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
		send_params = 
		{
			"user" : $localStorage.userid,
			"id" : id
		}			
		$http.post($rootScope.Host+'/block_user.php',send_params)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();
			$scope.FriendsArray.splice(index, 1);
			
			$scope.updateCount();
			
			$ionicPopup.alert({
			title: "משתמש הוסף לחסומים",
			template: ''
			});	

			
		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();

		});			
	}

	
	$scope.showFriendsModal = function(image)
	{
		$scope.FriendsImage = $rootScope.Host+image;
		
		$ionicModal.fromTemplateUrl('friends-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(friendsModal) {
		  $scope.friendsModal = friendsModal;
		  $scope.friendsModal.show();
		 });
	}
	
	$scope.closeFriendsModal = function()
	{
		$scope.friendsModal.hide();
	}
	
	
	$scope.openChat = function(id)
	{
		window.location ="#/app/chat/"+id+"/"+$localStorage.userid;
	}

	
	$rootScope.$on('refreshfriends', function(event, args) {
		
		$scope.getFriends();
	
	});

	
	
	$rootScope.$on('newfriend', function(event, args) {

		  $timeout(function() 
		  {
			//$scope.updateCount();
			$scope.getFriends();
		  }, 300);		
	});

	$rootScope.$on('newfollower', function(event, args) {
		
	  $timeout(function() 
	  {
		//$scope.updateCount();
		$scope.getFriends();
	  }, 300);	
	});	
	
	
	
	$ionicPlatform.on("resume", function(event) {
		$scope.getFriends();
	});	
	

	
	
})


.controller('BlockedCtrl', function($scope, $stateParams,$localStorage,$rootScope,$http,$ionicPopup,$ionicLoading,$ionicModal) {

	//$scope.navTitle='<p class="title-image">'+$localStorage.name+ ' ' +$localStorage.userid+   '</p>';
	$scope.navTitle='<p class="title-image">דיסקרטי</p>';
	$scope.BlockedImage = "";

	$scope.host = $rootScope.Host;
	
	$scope.CurrentUserId = $localStorage.userid;

	$scope.getBlocked = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
		send_params = 
		{
			"user" : $localStorage.userid
		}			
		$http.post($rootScope.Host+'/get_blocked.php',send_params)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();

			console.log("blocked : " , data)
			$scope.BlockedArray = data;
			

			
		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();

		});			
	}
	
	$scope.getBlocked();
	
	$scope.removeBlock = function(index,id,type)
	{
		//alert (type);

		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
		send_params = 
		{
			"user" : $localStorage.userid,
			"id" : id,
			"type" : type
		}			
		$http.post($rootScope.Host+'/remove_blocked.php',send_params)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();
			$scope.BlockedArray.splice(index, 1);
			$scope.updateCount();
			
			if (type == 0)
				$scope.alertText = "משתמש הוחזר לחברים";
			else
				$scope.alertText = "משתמש הוחזר לעוקבים";
			
			$ionicPopup.alert({
			title: $scope.alertText,
			template: ''
			});	

			
		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();

		});	
	}

	$scope.showBlockedModal = function(image)
	{
		$scope.BlockedImage = $rootScope.Host+image;
		
		$ionicModal.fromTemplateUrl('blocked-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(blockedModal) {
		  $scope.blockedModal = blockedModal;
		  $scope.blockedModal.show();
		 });
	}
	
	$scope.closeBlockedModal = function()
	{
		$scope.blockedModal.hide();
	}

	
})
.controller('ContactCtrl', function($scope, $stateParams,$localStorage,$rootScope,$http,$ionicPopup,$ionicLoading) {

	//$scope.navTitle='<p class="title-image">'+$localStorage.name+ ' ' +$localStorage.userid+   '</p>';
	$scope.navTitle='<p class="title-image">דיסקרטי</p>';


	$scope.fields = 
	{
		"name" : "",
		"phone" : "",
		"mail" : "",
		"desc" : ""
	}
	
	$scope.sendContact = function()
	{
		var emailRegex = /\S+@\S+\.\S+/;

				
		if ($scope.fields.name =="")
		{
			$ionicPopup.alert({
			title: 'יש להזין שם מלא',
			template: ''
			});	
		}
		else if ($scope.fields.phone =="")
		{
			$ionicPopup.alert({
			title: 'יש להזין טלפון',
			template: ''
			});	
		}
		else if ($scope.fields.mail =="")
		{
			$ionicPopup.alert({
			title: 'יש להזין מייל',
			template: ''
			});	
		}
		else if (emailRegex.test($scope.fields.mail) == false)
		{
			$ionicPopup.alert({
			title: 'מייל לא תקין נא להזין שוב',
			buttons: [{
			text: 'OK',
			type: 'button-positive',
			  }]
		   });	

		   $scope.fields.mail =  '';
		}	
		
		else if ($scope.fields.desc =="")
		{
			$ionicPopup.alert({
			title: 'יש להזין תוכן הפנייה',
			template: ''
			});	
		}		
		else
		{

			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
			send_params = 
			{
				"user" : $localStorage.userid,
				"name" : $scope.fields.name,
				"phone" : $scope.fields.phone,
				"mail" : $scope.fields.mail,
				"desc" : $scope.fields.desc,
				"send" : 1
				
			}			
			$http.post($rootScope.Host+'/contact.php',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();				
				
				$ionicPopup.alert({
				title: "תודה , הודעה נשלחה בהצלחה נחזור אליך בהקדם",
				template: ''
				});	

				
			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();

			});		
			
		}			
	}
	
	


})

.controller('DialerCtrl', function($scope, $stateParams,$localStorage,$rootScope,$http,$ionicPopup,$ionicLoading,$cordovaContacts) {

	//$scope.navTitle='<p class="title-image">'+$localStorage.name+ ' ' +$localStorage.userid+   '</p>';
	$scope.navTitle='<p class="title-image">דיסקרטי</p>';
	
	$scope.fields = 
	{
		"phone" : ""
	}
	
	
	$scope.dial = function(phone)
	{
		$scope.fields.phone += phone;
	}
	
	$scope.deleteBtn = function()
	{
		$scope.string = $scope.fields.phone;
		$scope.fields.phone =  $scope.string.substr(0, $scope.string.length-1); 
	}
	
	 $scope.selectContact = function () 
	 {
		$cordovaContacts.pickContact().then(function (contactPicked) {
		  //console.log ($scope.contact.phoneNumbers[0].value)
		  
		  //console.log (contactPicked.phoneNumbers[0].value)
		  
		  if (contactPicked.phoneNumbers[0].value)
			  $scope.fields.phone += contactPicked.phoneNumbers[0].value;
		  
		});
	  }	
	  
	

})


.controller('ChatCtr', function($scope, $stateParams,$localStorage,$rootScope,$http,$ionicPopup,$ionicLoading,$ionicModal,$ionicScrollDelegate,$timeout,$ionicPlatform) {

	$scope.user_local_storage = $localStorage.userid;
	$scope.chatArray = new Array();
	$scope.serverHost = $rootScope.Host;
	$scope.recipentId = $stateParams.UserOne;
	$scope.canChat = 0;
	//$scope.recipentId = $stateParams.UserTwo;
	//alert ($scope.recipentId)
	//alert ($scope.user_local_storage);

	$scope.chatFields = 
	{
		"chatbox" : ""	
	};
	
	/*
	if ($scope.userOne == $scope.user_local_storage)
	{
		$scope.recipentId = $scope.userTwo;
	}
	else
	{
		$scope.recipentId = $scope.userOne;
	}
	*/
		
		
	$rootScope.$on('newmessage', function(event, args) {

		

		  $timeout(function() 
		  {

		
			$scope.date = new Date()
			$scope.hours = $scope.date.getHours()
			$scope.minutes = $scope.date.getMinutes()
		
			if ($scope.hours < 10)
			$scope.hours = " " + $scope.hours
			
			if ($scope.minutes < 10)
			$scope.minutes = "0" + $scope.minutes
		
			$scope.time = $scope.hours+':'+$scope.minutes;

			
			$scope.chat  = {
				//"username" : args.username,
				"text" : args.text,
				"userimage" : $scope.serverHost+args.userimage,
				//"image" : $localStorage.image,
				"userid" : args.userid,
				"time" : $scope.time
			}
			
			//alert ($scope.chatArray.length);
			
			
			if ($scope.chatArray.length == 0)
				$scope.chatArray = new Array();		
			
						
			$scope.chatArray.push($scope.chat);
			$ionicScrollDelegate.scrollBottom();
			
			

			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
			"user" : $localStorage.userid,
			"recipent" : $scope.recipentId
			}
			//console.log(chat_params)
			$http.post($rootScope.Host+'/update_read_message.php',send_params)
			.success(function(data, status, headers, config)
			{
			})
			.error(function(data, status, headers, config)
			{

			});		

		  }, 300);		
	
			
	});
	
		

	
	$scope.getUserProfile = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

	    send_params = 
	    {
		"user" : $localStorage.userid,
		"recipent" : $scope.recipentId
		}
		//console.log(chat_params)
		$http.post($rootScope.LaravelHost+'/GetUserInfo',send_params)
		.success(function(data, status, headers, config)
		{
			$scope.recipentInfo = data[0];

		})
		.error(function(data, status, headers, config)
		{

		});			
	}
	
	$scope.getUserProfile();
	
	
	$scope.GetChatHistory = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

	    send_params = 
	    {
		"user" : $localStorage.userid,
	//	"user1" : $scope.userOne,
	//	"user2" : $scope.userTwo,		
		"recipent" : $scope.recipentId,
		"serverhost" : $rootScope.Host
		}
		//console.log(chat_params)
		$http.post($rootScope.LaravelHost+'/GetChatHistory',send_params)
		.success(function(data, status, headers, config)
		{
			console.log("chat history:", data);
			$scope.chatArray = data;
			$ionicScrollDelegate.scrollBottom();

		})
		.error(function(data, status, headers, config)
		{

		});			
	}
	
	$scope.GetChatHistory();

	
	
	
	$scope.sendChat = function()
	{
		if ($scope.chatFields.chatbox)
		{
			
			$scope.date = new Date()
			$scope.hours = $scope.date.getHours()
			$scope.minutes = $scope.date.getMinutes()
		
			if ($scope.hours < 10)
			$scope.hours = " " + $scope.hours
			
			if ($scope.minutes < 10)
			$scope.minutes = "0" + $scope.minutes
		
			$scope.time = $scope.hours+':'+$scope.minutes;

			
			//$ionicPlatform.ready(function() 
			//{
			//console.log($scope.chatArray);
			  send_params = 
			  {
				"user" : $localStorage.userid,
				//"user1" : $scope.userOne,
				//"user2" : $scope.userTwo,
				"recipent" : $scope.recipentId,
				//"sender" : $localStorage.userid,
				"text" : $scope.chatFields.chatbox,
				"time" : $scope.time,
				"gender" : $localStorage.sex, //  2 , 
				"serverhost" : $rootScope.Host
			
				}
				//alert(JSON.stringify(send_params));
				//console.log(chat_params)
				//$http.post($rootScope.LaravelHost+'/SendChatMessage',send_params)
				$http.post($rootScope.Host+'/send_chat_message.php',send_params)
				.success(function(data, status, headers, config)
				{
					$ionicScrollDelegate.scrollBottom();
					console.log("chat message", data);
					
					/*
					if (data.status == 1)
					{
						$scope.canChat = 1;
					}
					*/
					
					$scope.canChat = data.status;
					
					if (data.status == 0)
					{
						
						//alert ("expired membership");
						$scope.canChat = 0; 
					   var confirmPopup = $ionicPopup.confirm({
						 title: 'האם לעבור למסך רכישת מנוי?',
						 template: '',
						 cancelText: 'ביטול', 
						 okText: 'אישור'		 
					   });
					   

					   confirmPopup.then(function(res) {
						 if(res) 
						 {
							window.location ="#/app/payoptions";
						 }
					   });
					}
					
					
					
					$scope.chat  = {
						"username" : $localStorage.username,
						"text" : $scope.chatFields.chatbox,
						"userimage" : $scope.serverHost+$localStorage.image,
						//"image" : $localStorage.image,
						"userid" : $localStorage.userid,
						"time" : $scope.time
					}
					console.log("chatArray")
					console.log($scope.chatArray)
					
					if ($scope.chatArray.length == 0)
							$scope.chatArray = new Array();
						
					//alert ($scope.canChat);
					
					
					if ($scope.canChat == 1)
						$scope.chatArray.push($scope.chat);
					
					
					$scope.chatFields.chatbox = '';
					
					
					$ionicScrollDelegate.scrollBottom();

				

					
				})
				.error(function(data, status, headers, config)
				{

				});	
			//});
			


					
		}
	}
	
		
	$scope.enterPress = function(keyEvent)
	{
		if (keyEvent.which === 13)
		{
			$scope.sendChat();
		}
	}	
	
	
	$scope.chatBlur = function(e)
	{

	}

	
	


	$ionicPlatform.on("resume", function(event) {
		$scope.GetChatHistory();
	});

	
	
	
	
	
})


.controller('MessagesCtrl', function($scope, $stateParams,$localStorage,$rootScope,$http,$ionicPopup,$ionicLoading,$ionicModal,$ionicScrollDelegate,$timeout,$ionicPlatform) {

/*
	$scope.navTitle='<p class="title-image">דיסקרטי</p>';
	$scope.serverHost = $rootScope.Host;	

	$rootScope.$on('newmessage', function(event, args) {


		  $timeout(function() 
		  {

			$scope.getMessages();
			
		  }, 1000);		
	
			
	});
	
	$ionicPlatform.on("resume", function(event) {
		$scope.getMessages();
	});	
	
	$scope.getMessages = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

		
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	
					
		send_params = 
		{
			"user" : $localStorage.userid,
			"serverhost" : $rootScope.Host
	
		}
		//console.log(chat_params)
		$http.post($rootScope.LaravelHost+'/GetUserMessages',send_params)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();
			$scope.MessagesArray = data;
			
		   for(var i = 0 ;i < $scope.MessagesArray.length ;i++)
			{
				$scope.MessagesArray[i].visible = 0;
			}	

		
			console.log("messages",data);
		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});			
	}
	
	$scope.getMessages();
	
	$scope.openChat = function(item)
	{
		window.location ="#/app/chat/"+item.user1+"/"+item.user2;
	}
	
	$scope.blockChat = function(index,item)
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

		
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	
					
		send_params = 
		{
			"user" : $localStorage.userid,
			"id" : item.index
	
		}
		//console.log(chat_params)
		$http.post($rootScope.LaravelHost+'/DeleteChat',send_params)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();
			item.visible = 1;	

		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});	

		
		
	}
	
*/
})

.controller('PayOptionsCtrl', function($scope, $stateParams,$localStorage,$rootScope,$http,$ionicPopup,$ionicLoading,$ionicModal,$ionicScrollDelegate,$timeout,$ionicPlatform) {

	$scope.navTitle='<p class="title-image">דיסקרטי</p>';
	$scope.serverHost = $rootScope.Host;	
	
	//com.discreti.finalonemonth
  var productIds = ['finalonemonth','threemonths','sixmonths']; // <- Add your product Ids here

  var spinner = '<ion-spinner icon="dots" class="spinner-stable"></ion-spinner><br/>';

  
  $scope.savePurchase = function(productId)
  {
	 $ionicLoading.show({ duration: 3000, template: spinner + 'טוען...' });
	 

		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
		send_params = 
		{
			"user" : $localStorage.userid,
			"productid" : productId
		}			
		$http.post($rootScope.LaravelHost+'/savePurchase',send_params)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();

		});		 
	 
	 

  }
  
  $scope.loadProducts = function () {
    $ionicLoading.show({ duration: 3000, template: spinner + 'טוען מוצרים...' });
    inAppPurchase
      .getProducts(productIds)
      .then(function (products) {
        $ionicLoading.hide();
        $scope.products = products;
		//alert(JSON.stringify(products));
      })
      .catch(function (err) {
        $ionicLoading.hide();
        console.log(err);
		//alert(JSON.stringify(err));
      });
  };
  
	$scope.loadProducts();

  

		
		
  $scope.buy = function (productId) {

	//alert (productId)
    $ionicLoading.show({ template: spinner + 'מבצע רכישה...' });
    inAppPurchase
      .buy(productId)
      .then(function (data) {
		  
        console.log(JSON.stringify(data));
        console.log('consuming transactionId: ' + data.transactionId);
        return inAppPurchase.consume(data.type, data.receipt, data.signature);
      })
      .then(function () {
          var alertPopup = $ionicPopup.alert({
          title: 'רכישה בוצעה בהצלחה!',
			buttons: 
			[
			  {
				text: '<b>אישור</b>',
				type: 'button-positive',
			  }
			]
          //template: 'Check your console log for the transaction data'
        });
        console.log('consume done!');
        $ionicLoading.hide();
		$scope.savePurchase(productId);
      })
      .catch(function (err) {
        $ionicLoading.hide();
        console.log(err);
        $ionicPopup.alert({
          title: 'שגיאה ברכישה יש לנסות שוב',
          //template: 'Check your console log for the error details'
        });
      });

  };

  $scope.restore = function () {
    $ionicLoading.show({ template: spinner + 'משחזר רכישות...' });
    inAppPurchase
      .restorePurchases()
      .then(function (purchases) {
        $ionicLoading.hide();
        console.log(JSON.stringify(purchases));
        $ionicPopup.alert({
          title: 'שחזור רכישות בוצע בהצלחה!',
          //template: 'Check your console log for the restored purchases data'
        });
      })
      .catch(function (err) {
        $ionicLoading.hide();
        console.log(err);
        $ionicPopup.alert({
          title: 'שגיאות בשחזור רכישות יש לנסות שוב',
          //template: 'Check your console log for the error details'
        });
      });
  };	
	
})

.filter('lastseenDate', function ($rootScope) {
    return function (value) 
	{
			var splitLastSeen = '';
		
			//if (value.lastLogin)
			//splitLastSeen =  value.lastLogin.split(" ");
			//else if (value.date)
			//splitLastSeen =  value.date.split(" ");
			splitLastSeen =  value.split(" ");

			var date = splitLastSeen[0];
			var hour = splitLastSeen[1];
		
			var splitdate =  date.split("-");
			var splithour =  hour.split(":");
		
			return ''+splitdate[2]+'/'+splitdate[1]+'/'+splitdate[0]+' '+ splithour[0]+':'+splithour[1]+'';
		
		
    };
})

.filter('checkGender', function ($rootScope) {
    return function (value) 
	{
		var Img = "";
		
		if (value == 1)
		{
			Img = "img/settings/man.png";
		}
		else
		{
			Img = "img/settings/woman.png";
		}
		
		return Img;
    };
})


.directive('focusMe', function($timeout) {
  return {
    link: function(scope, element, attrs) {

      $timeout(function() {
        element[0].focus(); 
      });
    }
  };
})


.directive('imageonload', function($ionicLoading,$rootScope) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {
                //alert('image is loaded');
				$rootScope.$broadcast('imageloaded')
            });
        }
    };
});
